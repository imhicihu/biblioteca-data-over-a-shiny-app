![stability-workinprogress](https://img.shields.io/badge/stability-work_in_progress-lightgrey.svg)
![internaluse-green](https://img.shields.io/badge/Internal%20use%3A-stable-green.svg)
![issues-open](https://img.shields.io/badge/issues-open-green.svg)

# RATIONALE #

* Bibliographical data arranged to be queried over a [shiny](https://www.shinyapps.io/) app
* This repo is a living document that will grow and adapt over time

### What is this repository for? ###

* Quick summary
    - Porting our bibliographical data to be queried online
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - [R Studio](https://rstudio.com/products/rstudio/)
* Configuration
    - _In the making_. Further updates expected
* Dependencies
    - Node.js
* Database configuration
    - In the making
* Deployment instructions
    - _In the making_: updates expected

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/biblioteca-data-over-a-shiny-app/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/biblioteca-data-over-a-shiny-app/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/biblioteca-data-over-a-shiny-app/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - Install [R](https://cran.r-project.org/mirrors.html)
    - Install [Hyper](https://hyper.is/): this is a `console` app (`Terminal` in MacOSX operating system, `command` on Windows environments, _time-saver-logger_ of code commands with a lot of plugins and diverse enhancements
* Code review
    - Just follow our code guidelines
* Other guidelines
    - We are not fostering other guidelines. Simplicity is a must across all the process & procedure

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/biblioteca-data-over-a-shiny-app/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/biblioteca-data-over-a-shiny-app/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://img.shields.io/badge/Llicence-MIT-brightgreen.svg) 