* Accesibilidad
	 - [Colorblindly](https://chrome.google.com/webstore/detail/colorblindly/floniaahmccleoclneebhhmnjgdfijgg): Chrome extension that simulates colorblindness in a web browser
	 - Color palette
        - [Flat UI colors](https://flatuicolors.com/)
* Table converter
     - [Table convert](https://tableconvert.com/): converter to a plethora of formats
* Data validator
     - [Goodtables](https://goodtables.io/): frictionless tool to validate data, particularly spreadsheets
