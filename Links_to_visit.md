* https://github.com/rstudio/shiny-examples/tree/master/018-datatable-options
* https://shiny.rstudio.com/gallery/datatables-options.html
* https://medium.com/@CharlesBordet/how-to-deploy-a-shiny-app-on-aws-part-1-4893d0a7432f
* https://stackoverflow.com/questions/26799722/hosting-and-setting-up-own-shiny-apps-without-shiny-server
* https://towardsdatascience.com/how-to-host-a-r-shiny-app-on-aws-cloud-in-7-simple-steps-5595e7885722
* https://statistics.berkeley.edu/computing/shiny
* ~~http://abiyug.github.io/2016-04-05-shiny-web-app-hosting-on-github~~ (deprecated)
* [Running R on AWS](https://aws.amazon.com/blogs/big-data/running-r-on-aws/)
* [Hosting shiny on amazon EC2](deprecated!)
* [ShinyProxy](https://www.shinyproxy.io/): shiny apps running under a proxy server (take a look!)
* [StaticGen](https://www.staticgen.com/) // sinergy between Gatsby & Hugo (recipes). Made by Netlify